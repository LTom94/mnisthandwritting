package sample;

import java.awt.image.BufferedImage;

class MnistEntry {


    private BufferedImage img;
    private String label;
    private double value;


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public BufferedImage getImg() {
        return img;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public void setValue(double value) {
        this.value = value;
    }


}
