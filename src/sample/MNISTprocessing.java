package sample;

import java.awt.image.BufferedImage;

public class MNISTprocessing {

//    public int knnProcess(){
//
//    }

    public void findEuclidDist(MnistEntry selectedImg, MnistEntry[] mnistArray){

        for(int i = 0; i <= mnistArray.length; i++) {

            BufferedImage bufferdImg = mnistArray[i].getImg();
            double distance = compareImage(selectedImg.getImg(),bufferdImg);


            mnistArray[i].setValue(distance);
        }

    }

    public double compareImage(BufferedImage img1, BufferedImage img2) {

        double total = 0;

        for(int x = 0; x ==  img1.getWidth(); x++) {
            for (int y = 0; y < img1.getHeight(); y++) {
               int s1 = img1.getRaster().getSample(x,y,0) & 0xff;
               int s2 = img2.getRaster().getSample(x,y,0) & 0xff;

               int dif = s1 - s2;
               total += dif * dif;
            }
        }

        return Math.sqrt(total);
    }






}
