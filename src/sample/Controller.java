package sample;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.Stage;

import static javafx.embed.swing.SwingFXUtils.toFXImage;


public class Controller implements Initializable {

    public File trainingdata;
    public File testdata;

    public File trainingLabels;
    public File testdataLabels;

    private MnistEntry[] testData;
    private MnistEntry[] trainingData;

    public Label path1_lbl;
    public Label path2_lbl;
    public Label path3_lbl;
    public Label path4_lbl;
    public Label rndDigit_lbl;


    public Canvas rndDigit_cnvs;
    public Canvas userInput_cnvs;

    public ImageView rndDigit_imgv;

    public void FileChooser(ActionEvent actionEvent) {
        final Button activatedButton = (Button)actionEvent.getSource();
        String buttonTitle = activatedButton.getId();
        try {
            switch (buttonTitle) { ///
                case "SelectTraining_btn":
                    trainingdata = FileHandler.OpenFileHandler("IDX3-UBYTE File", "*.idx3-ubyte");
                    path1_lbl.setText(trainingdata.getName());
                    break;
                case "SelectTest_btn":
                    testdata = FileHandler.OpenFileHandler("IDX3-UBYTE File", "*.idx3-ubyte");
                    path3_lbl.setText(testdata.getName());
                    break;
                case "SelectLbl1_btn":
                    trainingLabels = FileHandler.OpenFileHandler("IDX1-UBYTE File", "*.idx1-ubyte");
                    path2_lbl.setText(trainingLabels.getName());
                    break;
                case "SelectLbl2_btn":
                    testdataLabels = FileHandler.OpenFileHandler("IDX1-UBYTE File", "*.idx1-ubyte");
                    path4_lbl.setText(testdataLabels.getName());
            }
        }catch (NullPointerException e) {
            System.out.print("No file chosen");
        }
    }

    public void FileLoader(ActionEvent actionEvent) throws IOException, InterruptedException {
        final Button  activatedButton = (Button)actionEvent.getSource();
        String buttonTitle = activatedButton.getId();
        switch (buttonTitle) {
            case "LoadTraining_btn":
                trainingData = FileHandler.LoadFileHandler(trainingdata,trainingLabels);
                break;
            case "LoadTest_btn":
                testData = FileHandler.LoadFileHandler(testdata,testdataLabels);
                break;
        }
    }

    public void RandomDigit(ActionEvent actionEvent) {

//        int rnd = new Random().nextInt(testData.length);
//        BufferedImage img = testData[rnd].getImg();
//        Image img2 = SwingFXUtils.toFXImage(img, null);
//
//        rndDigit_imgv.setImage(img2);



// Buffered Image to Canvas
        int rnd = new Random().nextInt(testData.length);
        System.out.println("rnd =\t"+ rnd);

        BufferedImage buffImg = testData[rnd].getImg();
        WritableImage img = SwingFXUtils.toFXImage(buffImg, null);
        GraphicsContext gc = rndDigit_cnvs.getGraphicsContext2D();

       //gc.clearRect(0, 0, rndDigit_cnvs.getWidth(),rndDigit_cnvs.getHeight());

        gc.drawImage(img,rndDigit_cnvs.getScaleX(),rndDigit_cnvs.getScaleY());


        rndDigit_lbl.setText("Test Image Label:\t" + testData[rnd].getLabel());
    }


    public void Test(ActionEvent actionEvent) {
        System.out.print("hello");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        GraphicsContext gc = userInput_cnvs.getGraphicsContext2D();

        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, 250,250 );


        userInput_cnvs.setOnMouseDragged(event -> {double size = 10;
        double x = event.getX() - size / 2;
        double y = event.getY() - size /2;

        gc.setLineCap(StrokeLineCap.ROUND);
        gc.setFill(Color.WHITE);
        gc.fillRect(x, y, size,size );
        });
    }

    public void clear(ActionEvent actionEvent) {
        GraphicsContext gc = userInput_cnvs.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, 250, 250);
    }
}
