package sample;

import javafx.scene.control.Alert;
import javafx.stage.FileChooser;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;

import java.io.*;
import java.util.stream.Stream;

public class FileHandler extends Controller {

    public static File OpenFileHandler(String Description, String Extension) throws NullPointerException {

        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(Description, Extension));
        File selectedFile = fc.showOpenDialog(null);

        return selectedFile;
    }

    // merge Test data / test Labels
    // merge Training data / training Labels

    public static MnistEntry[] LoadFileHandler(File imgData, File lblData) throws IOException {

        Thread thread = new Thread();
        thread.start();
        //DatainputStream

        DataInputStream in_stream_images = null;
        DataInputStream in_stream_labels = null;

        try {
            in_stream_images = new DataInputStream(new FileInputStream(imgData));
            in_stream_labels = new DataInputStream(new FileInputStream(lblData));
        } catch (NullPointerException e) {
            if (Stream.of(imgData, lblData).anyMatch(x -> x == null)) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Missing Files");
                alert.setContentText("Please Load Data");
                alert.showAndWait();
                return null;
            }
        }


        int labels_start_code = in_stream_labels.readInt();
        System.out.println("Label Start Code is:\t" + labels_start_code);

        int img_start_code = in_stream_images.readInt();
        System.out.println("Image Start Code is:\t" + img_start_code);

        int number_of_imgs = in_stream_images.readInt();
        int number_of_labels = in_stream_labels.readInt();
        System.out.println("Number of images:\t" + number_of_imgs + " Number of labels:\t" + number_of_labels);

        int img_width = in_stream_images.readInt();
        int img_height = in_stream_images.readInt();



        int[][] imageDataArray = new int[img_width][img_height];

        MnistEntry[] tempArray = new MnistEntry[number_of_imgs];


        for (int i = 0; i < tempArray.length; i++) {
            MnistEntry data = new MnistEntry();

            BufferedImage tempImg = new BufferedImage(img_width, img_height, BufferedImage.TYPE_BYTE_GRAY);

            //set label of MNIST Entry
            data.setLabel(Integer.toString(in_stream_labels.readUnsignedByte()));


            for (int x = 0; x < img_width; x++) {
                for (int y = 0; y < img_height; y++) {
                    imageDataArray[x][y] = in_stream_images.readUnsignedByte();
                    //System.out.print(imageDataArray[x][y]+ ",");
                    tempImg.setRGB(x, y, imageDataArray[x][y]);
                }
                //System.out.println();
            }

            //System.out.println("image:\t" + i);
            data.setImg(tempImg);
            tempArray[i] = data;
        }

        //System.out.println("tempArray Size:\t" + tempArray);
        System.out.println("Ayyyy");
        return tempArray;
    }
}

